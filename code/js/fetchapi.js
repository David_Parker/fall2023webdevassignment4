const fetchData = {
    url1Data: [],
    days: [0, 8, 16],
    url2Data: {},
    city: '',
    country: '',
    latitude: '',
    longitude: '',
    population: '',
    state: '',
    sunrise: '',
    sunset: '',
    weatherInfo: [],
    apiKey: 'f6137a16bd99afe69a00c55af818c4b7',
    cityURL() {
        return `https://api.openweathermap.org/geo/1.0/direct?q=${this.city},,${this.country}&limit=5&appid=${this.apiKey}`;
    },
    weatherURL() {
        return `https://api.openweathermap.org/data/2.5/forecast?lat=${this.latitude}&lon=${this.longitude}&appid=${this.apiKey}&units=metric`;
    },

    /**
     * Fetches information from api to get base city information
     */
    getAllCititesInfo: function getAllCitiesInfo() {
        this.setCity();
        this.country = '';
        fetch(this.cityURL())
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error(`Something went wrong... ${response.status}`);
            })
            .then((data) => {
                this.url1Data = data;
                this.setUrl1Data();
                this.getWeatherInfo();
            })
            .catch((error) => {
                console.error(error);
            });
    },

    /**
     * Fetches the information from the api about the city weather
     */
    getWeatherInfo: function getWeatherInfo() {
        fetch(this.weatherURL())
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error(`Something went wrong... ${response.status}`);
            })
            .then((data) => {
                this.url2Data = data;
                this.setUrl2Data();
                this.setThreeDayWeatherInfo();
                this.showData();
            })
            .catch((error) => {
                console.error(error);
            });
    },

    /**
     * Sets the city field of the object
     */
    setCity: function setCity() {
        this.city = document.querySelector('#cityName').value;
    },

    /**
     * Sets the data from url1
     */
    setUrl1Data: function setUrl1Data() {
        let position = 0;
        const selectedCountry = document.querySelector('#country-select').value;
        if (selectedCountry !== '') {
            position = selectedCountry;
        }
        this.country = this.url1Data[position].country;
        this.latitude = this.url1Data[position].lat;
        this.longitude = this.url1Data[position].lon;
        this.state = this.url1Data[position].state;
    },

    /**
     * Sets the common url2 information of the city
     */
    setUrl2Data: function setUrl2Data() {
        this.population = this.url2Data.city.population;
        this.sunrise = utilities.convertUnixHourMinute(this.url2Data.city.sunrise);
        this.sunset = utilities.convertUnixHourMinute(this.url2Data.city.sunset);
    },

    /**
     * Sets the information for the next three days
     */
    setThreeDayWeatherInfo: function setThreeDayWeatherInfo() {
        this.weatherInfo = [];
        this.days.forEach((day) => {
            this.weatherInfo.push(
                {
                    dateTime: new Date(this.url2Data.list[day].dt * 1000).toLocaleString('en-CA'),
                    icon: `https://openweathermap.org/img/wn/${this.url2Data.list[day].weather[0].icon}@2x.png`,
                    description: this.url2Data.list[day].weather[0].description,
                    temp: this.url2Data.list[day].main.temp,
                    feelsLike: this.url2Data.list[day].main.feels_like,
                    humidity: this.url2Data.list[day].main.humidity,
                    wind: this.url2Data.list[day].wind.speed,
                },
            );
        });
    },

    /**
     * Displays all 3 days of weather info
     */
    showData: function showData() {
        document.querySelector('#weather-info').innerHTML = '';
        let counter = 0;
        this.days.forEach(() => {
            this.htmlWeatherInfoDay(counter);
            counter += 1;
        });
        this.countryOptionDisplay();
        this.showCityInfo();
        this.showCityCoordinates();
    },

    /**
     * Displays the population, sunset, and sunrise of the city
     */
    showCityInfo: function showCityInfo() {
        const population = document.querySelector('#population');
        const sunrise = document.querySelector('#sunrise');
        const sunset = document.querySelector('#sunset');
        population.value = this.population;
        sunrise.value = this.sunrise;
        sunset.value = this.sunset;
    },

    /**
     * Displays the latitude and longitude of the city
     */
    showCityCoordinates: function showCityCoordinates() {
        const latitude = document.querySelector('#latitudeNumber');
        const longitude = document.querySelector('#longitudeNumber');
        latitude.value = this.latitude;
        longitude.value = this.longitude;
    },

    /**
     * Adds the country tags to the select data country/state
     */
    countryOptionDisplay: function countryOptionDisplay() {
        const selectElem = document.querySelector('#country-select');
        let counter = 0;
        utilities.deleteChildNodes(selectElem);
        this.url1Data.forEach((element) => {
            utilities.createElement('option', { parent: selectElem, textContent: `${element.country}/${element.state}`, value: counter });
            counter += 1;
        });
    },

    /**
     * Creates a single day panel of weather info
     * @param {Number} position Object of the weather data
     */
    htmlWeatherInfoDay: function htmlWeatherInfoDay(position) {
        const mainContainer = document.querySelector('#weather-info');
        const dayContainer = utilities.createElement('div', { parent: mainContainer });

        const dateContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: dateContainer, textContent: this.weatherInfo[position].dateTime });
        const imgContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('img', { parent: imgContainer, src: this.weatherInfo[position].icon });
        const descriptionContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: descriptionContainer, textContent: 'Description' });
        utilities.createElement('h4', { parent: descriptionContainer, textContent: this.weatherInfo[position].description });
        const tempContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: tempContainer, textContent: 'Temperature' });
        utilities.createElement('h4', { parent: tempContainer, textContent: `${this.weatherInfo[position].temp}°C`, class: 'tempValues' });
        const feelsLikeContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: feelsLikeContainer, textContent: 'Feels-Like' });
        utilities.createElement('h4', { parent: feelsLikeContainer, textContent: `${this.weatherInfo[position].feelsLike}°C`, class: 'tempValues' });
        const humidityContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: humidityContainer, textContent: 'Humidity' });
        utilities.createElement('h4', { parent: humidityContainer, textContent: `${this.weatherInfo[position].humidity}%` });
        const windContainer = utilities.createElement('div', { parent: dayContainer });
        utilities.createElement('h4', { parent: windContainer, textContent: 'Wind Speed' });
        utilities.createElement('h4', { parent: windContainer, textContent: `${this.weatherInfo[position].wind}m/s` });
    },
};

/**
 * Initializes all the event listeners
 */
function init() {
    const submitButton = document.querySelector('#getWeatherButton');
    const selectCountry = document.querySelector('#country-select');

    submitButton.addEventListener('click', () => {
        fetchData.getAllCititesInfo();
    });

    selectCountry.addEventListener('change', () => {
        fetchData.setUrl1Data();
    });
}

init();
