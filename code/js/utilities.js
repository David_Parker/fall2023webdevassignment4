const utilities = {
    /**
     * Deletes all the childNodes of the parent element
     * @param {Element} parent
     */
    deleteChildNodes: function deleteChildNodes(parent) {
        while (parent.hasChildNodes()) {
            parent.removeChild(parent.childNodes[0]);
        }
    },

    /**
     * Creates an element using an object
     * @param {String} tagName
     * @param {Object} obj
     */
    createElement: function createElement(tagName, obj) {
        const elem = document.createElement(tagName);
        const parent = obj.parent;
        if (obj.textContent !== undefined) {
            elem.textContent = obj.textContent;
        }
        if (obj.src !== undefined) {
            elem.src = obj.src;
        }
        if (obj.value !== undefined) {
            elem.value = obj.value;
        }
        if (obj.class !== undefined) {
            elem.className = obj.class;
        }
        parent.appendChild(elem);
        return elem;
    },

    /**
     * Converts unix time to normal time
     * @param {Number} unixTime
     * @returns Formatted
    */
    convertUnixHourMinute: function convertUnixTime(unixTime) {
        const time = new Date(unixTime * 1000);
        const hours = time.getHours();
        const minutes = time.getMinutes();
        return `${hours}:${minutes}`;
    },
};
